from django.contrib import admin
from .models.services import Service
from .models.users import User
from .models.technicians import Technician

# Register your models here.
admin.site.register(Service)
admin.site.register(User)
admin.site.register(Technician)
