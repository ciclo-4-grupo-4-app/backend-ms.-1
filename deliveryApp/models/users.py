from django.db import models

class User(models.Model):
     id = models.AutoField(primary_key=True)
     names = models.CharField(max_length=60)
     lastnames = models.CharField(max_length=60)
     locality = models.CharField(max_length=40)
     address = models.CharField(max_length=100)
     telephone = models.CharField(max_length=20)
     email = models.CharField(max_length=40)