from django.db import models
from django.contrib.postgres.fields import ArrayField

class Technician(models.Model):
     id = models.AutoField(primary_key=True)
     names = models.CharField(max_length=60)
     lastnames = models.CharField(max_length=60)     
     address = models.CharField(max_length=100)
     telephone = models.CharField(max_length=20)
     email = models.CharField(max_length=40)
     experience = models.CharField(max_length=500)
     covered_serv = models.CharField(max_length=20)
     covered_localities = ArrayField(models.CharField(max_length=40))
     covered_modules = ArrayField(models.CharField(max_length=20))
     covered_categories = ArrayField(models.CharField(max_length=20))