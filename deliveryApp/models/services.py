from django.db import models
from .users import User
from .technicians import Technician

class Service(models.Model):
    id = models.AutoField(primary_key=True)
    user_id = models.ForeignKey(User, to_field='id', on_delete=models.CASCADE)
    tech_id = models.ForeignKey(Technician, to_field='id', on_delete=models.CASCADE)
    module = models.CharField(max_length=20)
    dom_service = models.CharField(max_length=20)
    category = models.CharField(max_length=20)
    description = models.CharField(max_length=20)
    app_date = models.DateField()