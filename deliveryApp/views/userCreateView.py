from rest_framework import serializers, status, views
from rest_framework.response import Response
from deliveryApp.serializers.userSerializer import UserSerializer
from deliveryApp.serializers.serviceSerializer import ServiceSerializer
from deliveryApp.models.users import User

class UserCreateView(views.APIView):
    
    def post(self, request):

        '''
            {
            "names": "JUAN",
            "lastnames" : "PEREZ",
            "locality":  "SUBA",
            "address": "CALLE 1",
            "telephone": "315574",
            "email": "jp@gmail.com",

            
            "tech_id": 1,
            "module": "C",
            "dom_service": "C",
            "category": "CV",
            "description": "C",
            "app_date": "2021-10-14"
            }
        '''

        serializer = UserSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
       
        create = serializer.save()

        # Retorna id del registro insertado
        getId = create.id

        # Creación de objeto User
        user_id = User.objects.get(id=getId)

        # Creación de servicio
        if(getId):
            serializer = ServiceSerializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            create = serializer.save(user_id = user_id)

        getIdService = create.id    

        return Response(data={"message": "Registro de usuario y servicio satisfactorio", "service_id" : getIdService}, status=status.HTTP_201_CREATED)