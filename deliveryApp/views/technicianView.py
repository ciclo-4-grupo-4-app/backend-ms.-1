from rest_framework import serializers, status, views
from rest_framework.response import Response
from deliveryApp.serializers.technicianSerializer import TechnicianSerializer
from deliveryApp.models.technicians import Technician
from django.forms.models import model_to_dict

class TechnicianView(views.APIView):

    def get(self, request):
        locality = request.data['localidad']
        service = request.data['servicio']
        category = request.data['categoria']
       
        technicians = [ model_to_dict(tech) for tech in Technician.objects.all() 
                        if locality in tech.covered_localities and service in tech.covered_serv and category in tech.covered_categories]
        return Response(technicians, status=status.HTTP_200_OK)        

    def post(self, request):
        serializer = TechnicianSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(data={'Technician successfully created'}, status=status.HTTP_201_CREATED)