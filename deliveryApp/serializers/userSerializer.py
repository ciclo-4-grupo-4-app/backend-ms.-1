from deliveryApp.models.users import User
from rest_framework import serializers

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id','names','lastnames','locality','address','telephone','email']        