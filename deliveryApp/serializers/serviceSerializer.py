from deliveryApp.models.services import Service
from rest_framework import serializers

class ServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Service
        fields = ['id','tech_id','module','dom_service','category','description','app_date']