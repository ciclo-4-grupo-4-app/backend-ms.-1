from deliveryApp.models.technicians import Technician
from rest_framework import serializers

class TechnicianSerializer(serializers.ModelSerializer):
    class Meta:
        model = Technician
        fields = ['id','names','lastnames','address','telephone','email','experience','covered_serv',
                  'covered_localities','covered_modules','covered_categories']        